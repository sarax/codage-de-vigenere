#include<stdio.h>
#include<stdlib.h>
#include <string.h>
int estMinuscule(char c){
    if(c>='a' && c<='z')
    return 1;
    else
        
        return 0;
}
int estMajuscule(char c){
    if(c>='A' && c<='Z')
    return 1;
    else
        
        return 0;
}

int main(int argc,char *argv[]){
    /*char decalage[]="java";
    char j=decalage[2]-'a'+1;
    printf("%d\n",j);
    char jj=16+(22-'a'+1);
    jj=jj+((int)(-jj/26) +1)*26;
    printf("%d\n",jj);
    */
    ////////////////////////////////////////
    char *decalage=argv[2];
    size_t taille = strlen(decalage);
    //printf("%ld\n",taille);
    /////////////////////////////::
    char *fichier_entree=argv[1];
    char *fichier_sortie=argv[3];
    FILE *f;
    FILE *s;
    f=fopen(fichier_entree, "rb");
    if(f==NULL){
        perror("Erreur lors de l'ouverture du fichier\n");
        return 0;
    }
    s=fopen(fichier_sortie, "wb");
    if(s==NULL){
        perror("Erreur lors de l'ouverture du fichier\n");
        return 0;
    }
    int i=0;
    int une;

    int caractere;
    while((caractere=fgetc(f))!=EOF){
        if(estMinuscule(caractere)|| estMajuscule(caractere)){
            char base= estMinuscule(caractere) ? 'a' : 'A';
            char ali= estMinuscule(decalage[i]) ? 'a' : 'A';
            une=decalage[i]-ali+1;
            if(une<0){
                une=une+((int)(-une/26) +1)*26;
                //caractere=caractere+une;
                caractere = (caractere - base + une) % 26 + base;
                fputc(caractere, s);
            }else{
                caractere = (caractere - base + une) % 26 + base;
                //caractere=caractere+une;
                fputc(caractere, s);
            }
                
            i++;
            if(i>taille-1){
                i=0;
            }
        }else
            fputc(caractere, s);
    }
   
    fclose(f);
    fclose(s);
    return 0;
}