#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>
int estMinuscule(char c){
    if(c>='a' && c<='z')
    return 1;
    else
        
        return 0;
}
int estMajuscule(char c){
    if(c>='A' && c<='Z')
    return 1;
    else
        
        return 0;
}
void normaliserFichier(FILE *in, FILE *out){
    int caractere;

    while((caractere=fgetc(in))!=EOF){
        if( estMinuscule(caractere)){
            fputc(caractere,out);

        }
        if(estMajuscule(caractere)){
            fputc(tolower(caractere),out);

        }
    }
}
double IndiceCoincidence(FILE *in){
    
    int tab[26]={};
    int i=0;
    int caractere;

        
    while((caractere=fgetc(in))!=EOF){
        
        tab[caractere-'a']++;
    }
    double indiceDeCoincidence=0;
    double num=0,nbreDelettre=0;
    for(i=0;i<26;i++){
        num+= tab[i]*(tab[i]-1);
        nbreDelettre+=tab[i];
    }
    printf("totalLettre=%f\n",nbreDelettre);
    indiceDeCoincidence =num/(nbreDelettre*(nbreDelettre-1));
    printf("Indice=%f\n",indiceDeCoincidence);
    //printf("Na=%d\n",tab[0]);
    //printf("Nb=%d\n",tab[1]);
    printf("Nb=%d\n",tab[25]);

    
    return indiceDeCoincidence;
}
void decouperFichier(FILE *in, FILE *out, int longueur,int debut){
    
    int caractere;
    while((caractere=fgetc(in))!=EOF){
        fputc(caractere,out);

        fseek(in, longueur-1, SEEK_CUR);
    }    
}
int longueurMotPasseVigenere(FILE *in){
    int i=0;
    double a;
    char *nomFichierSortie = "fichier_sortielongueur.txt";
    FILE *fichierSortie;
   
    
    for(i=1;i<31;i++){
        fichierSortie=fopen(nomFichierSortie, "wb");
        decouperFichier(in, fichierSortie,i,0);
        fclose(fichierSortie);

        fichierSortie=fopen(nomFichierSortie, "rb");
        a=IndiceCoincidence(fichierSortie);
        fclose(fichierSortie);
        if(a>=0.065 && a<=0.084){
            //i est la longueur du mot_de_passe
            
            return i;
        }
    
        rewind(in);
       
        
    }
    printf("On a pas trouvé.Dommage\n");
    return 0;
}
char prochaineLettre(FILE *in){
    char caractereNul='\0';
    if(in==NULL){
        perror("erreur lors de l'ouverture du fichier");
        return 1;
    }
    int caractere;
    caractere=fgetc(in);

    while(caractere!=EOF){
        if(estMinuscule(caractere) || estMajuscule(caractere)){
        
        //printf("%c\n",caractere);
        return caractere;
        }
        else
            caractere=fgetc(in);
    }
    return caractereNul;
    
}
  
char lettreLaPlusFrequente(FILE *in) {
    rewind(in);
    char lettreFreq = '\0'; 
    int compteurMax = 0;
    
    if (in == NULL) {
        perror("Echec Ouverture");
        return '\0';
    }
   int compteur[26] = {0}; // Un tableau pour compter les occurrences de chaque lettre
    
    char c;
    while ((c = prochaineLettre(in)) != '\0') {

        int index = c - 'a';
        compteur[index]++;
        if (compteur[index] > compteurMax) {
            compteurMax = compteur[index];
            lettreFreq = c;
        }
    }
    
    if (lettreFreq != '\0') {
        printf("La lettre la plus fréquente est : %c\n", lettreFreq);
    } else {
        printf("Le fichier est vide ou ne contient aucune lettre.\n");
    }
    
    return lettreFreq;
}
void decouperFichiercrypte(FILE *in, FILE *out, int longueur,int debut){
    
    int caractere;
    while((caractere=fgetc(in))!=EOF){
        fputc(caractere,out);

        fseek(in, longueur, SEEK_CUR);
    }    
}
char* trouver_pass(FILE *crypte, int longueur) {
    char *ptrx = malloc(sizeof(char)*(longueur+1));
    int i=0;
   
    
    
    char *nomFichierSortie2x = "fichier_sortie2x.txt";
    FILE *fichierSortie2x ;
    while(i<longueur){
        fichierSortie2x = fopen(nomFichierSortie2x, "wb");
        fseek(crypte, i, SEEK_SET);
        decouperFichiercrypte(crypte,fichierSortie2x,longueur-1,0);

        fclose(fichierSortie2x);
        fichierSortie2x = fopen(nomFichierSortie2x, "rb");

        ptrx[i]=lettreLaPlusFrequente(fichierSortie2x);
        i++;
        rewind(crypte);

        fclose(fichierSortie2x);
    }
    ptrx[i]='\0';
    return ptrx;
}
void vigenere(FILE *in, char* mot_de_passe, FILE *out){
  
    size_t taille = strlen(mot_de_passe);
    fseek(in,0,SEEK_SET);
    int i=0;
    

    int caractere;
    while((caractere=fgetc(in))!=EOF){
        
            if (estMinuscule(caractere) || estMajuscule(caractere)) {
            // Vérifier si le caractère est une lettre non accentuée
            int decalage='e'-mot_de_passe[i];
            if(decalage<0){
                decalage=decalage+((-decalage/26) +1)*26;
            }    
            char base = estMajuscule(caractere) ? 'A' : 'a';
            caractere = (caractere - base + decalage) % 26 + base;
            fputc(caractere, out);
               
            i++;
            if(i==taille){
                i=0;
            }
            }else
            fputc(caractere, out);
    }

}

int main (int argc, char *argv[]){
    char *monfichier=argv[1];
    FILE *f=fopen(monfichier, "rb");

    char *monfichiers=argv[2];
    FILE *out=fopen(monfichiers, "wb");

    char *nomFichierSortie = "fichier_sortieali.txt";
    FILE *fichierSortie = fopen(nomFichierSortie, "wb");

    normaliserFichier(f,fichierSortie);
    fclose(fichierSortie);
    fichierSortie = fopen(nomFichierSortie, "rb");

    int longueur=0;
    longueur=longueurMotPasseVigenere(fichierSortie);
    printf("la longueur est :%d\n",longueur);
    if(longueur==0){
    printf("la longueur du mot_de_passe n'a pas été trouvé. Un fichier de sortie ne peut être créer.\n");
    return 0;
    }
    fclose(fichierSortie);
    fichierSortie = fopen(nomFichierSortie, "rb");
    
    char *ptre ;
    ptre=trouver_pass(fichierSortie, longueur);
    //printf("Dans main:%c",ptre[0]);
    rewind(f);
    vigenere(f,ptre,out);
    
    return 0;
}
