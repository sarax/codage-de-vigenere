#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

int estMinuscule(char c){
    if(c>='a' && c<='z')
    return 1;
    else
        
        return 0;
}
int estMajuscule(char c){
    if(c>='A' && c<='Z')
    return 1;
    else
        
        return 0;
}

char prochaineLettre(FILE *in){
    char caractereNul='\0';
    if(in==NULL){
        perror("erreur lors de l'ouverture du fichier");
        return 1;
    }
    int caractere;
    caractere=fgetc(in);

    while(caractere!=EOF){
        if(estMinuscule(caractere) || estMajuscule(caractere)){
        
        //printf("%c\n",caractere);
        return caractere;
        }
        else
            caractere=fgetc(in);
    }
    return caractereNul;
    
}
   
char lettreLaPlusFrequente(FILE *in) {
    rewind(in);
    char lettreFreq = '\0'; 
    int compteurMax = 0;
    
    if (in == NULL) {
        perror("Echec Ouverture");
        return '\0';
    }
   int compteur[26] = {0}; // Un tableau pour compter les occurrences de chaque lettre
    
    char c;
    while ((c = prochaineLettre(in)) != '\0') {

        int index = c - 'a';
        compteur[index]++;
        if (compteur[index] > compteurMax) {
            compteurMax = compteur[index];
            lettreFreq = c;
        }
    }
    
    if (lettreFreq != '\0') {
        printf("La lettre la plus fréquente est : %c\n", lettreFreq);
    } else {
        printf("Le fichier est vide ou ne contient aucune lettre.\n");
    }
    
    return lettreFreq;
}


void decodageCesar(FILE *in, FILE *out){
    char lettreFreq=lettreLaPlusFrequente(in);
    int decalage='e'-lettreFreq;
    fseek(in,0,SEEK_SET);

    if(decalage<0){
        decalage=decalage+((-decalage/26) +1)*26;
    }
    printf("%d\n",decalage);

    int caractere;
    while ((caractere = fgetc(in)) != EOF) {
        if (estMinuscule(caractere) || estMajuscule(caractere)) {
            // Vérifier si le caractère est une lettre non accentuée
            char base = estMajuscule(caractere) ? 'A' : 'a';
            caractere = (caractere - base + decalage) % 26 + base;
            fputc(caractere, out);
        }
        else
        fputc(caractere, out);
    }
}
void normaliserFichier(FILE *in, FILE *out){
    int caractere;

    while((caractere=fgetc(in))!=EOF){
        if( estMinuscule(caractere)){
            fputc(caractere,out);

        }
        if(estMajuscule(caractere)){
            fputc(tolower(caractere),out);

        }
    }
}
void decouperFichier(FILE *in, FILE *out, int longueur,int debut){
    
    int caractere;
    
   
    while((caractere=fgetc(in))!=EOF){
        fputc(caractere,out);

        fseek(in, longueur, SEEK_CUR);
    }
    
    
}
void vigenere(FILE *in, char* mot_de_passe, FILE *out){
  
    size_t taille = strlen(mot_de_passe);
    fseek(in,0,SEEK_SET);
    int i=0;
    

    int caractere;
    while((caractere=fgetc(in))!=EOF){
        
            if (estMinuscule(caractere) || estMajuscule(caractere)) {
            // Vérifier si le caractère est une lettre non accentuée
            int decalage='e'-mot_de_passe[i];
            if(decalage<0){
                decalage=decalage+((-decalage/26) +1)*26;
            }    
            char base = estMajuscule(caractere) ? 'A' : 'a';
            caractere = (caractere - base + decalage) % 26 + base;
            fputc(caractere, out);
               
            i++;
            if(i==taille){
                i=0;
            }
            }else
            fputc(caractere, out);
    }

}
char* trouver_pass(FILE *crypte, int longueur) {
    char *ptr = malloc(sizeof(char)*(longueur+1));
    int i=0;
   
    char *nomFichierSortie = "fichier_sortie.txt";
    FILE *fichierSortie = fopen(nomFichierSortie, "wb");
    normaliserFichier(crypte,fichierSortie);
    fclose(fichierSortie);
    fichierSortie = fopen(nomFichierSortie, "rb");
    
    char *nomFichierSortie2 = "fichier_sortie2.txt";
    FILE *fichierSortie2 ;
    while(i<longueur){
        fichierSortie2 = fopen(nomFichierSortie2, "wb");
        fseek(fichierSortie, i, SEEK_SET);
        decouperFichier(fichierSortie,fichierSortie2,longueur-1,0);

        fclose(fichierSortie2);
        fichierSortie2 = fopen(nomFichierSortie2, "rb");

        ptr[i]=lettreLaPlusFrequente(fichierSortie2);
        i++;
        rewind(fichierSortie);
    }
    ptr[i]='\0';
    
    
    return ptr;
}



int main(int argc, char *argv[]){
  
    char *entree=argv[1];
    int taille=atoi(argv[2]);
    char *sortie=argv[3];
    //char *decouper=argv[3];
    FILE *in=fopen(entree,"rb");
    FILE *out=fopen(sortie,"wb");
    //FILE *fichier_decouper=fopen(decouper,"wb");
    

    
    char *ptre ;
    ptre=trouver_pass(in, taille);
    //printf("%c",ptre[0]);
    /*printf("%c",mot[1]);
    printf("%c",mot[2]);
    printf("%c",mot[3]);
    printf("%c\n",mot[4]);*/
    rewind(in);
    vigenere(in,ptre,out);
    


    fclose(in);
  
    return 0;
}